# QMANOVA

This is the repository or the R-simulation code of the QMANOVA: [Quantile-based MANOVA: A new tool
for inferring multivariate data in factorial
designs](https://www.sciencedirect.com/science/article/pii/S0047259X23000921)

There is an R-function to run the method in [dataanalysis](./dataanalysis).

## Authors

Marléne Baumeister, [baumeister@statistik.tu-dortmund.de](mailto:baumeister@statistik.tu-dortmund.de)

Marc Ditzhaus, [marc.ditzhaus@ovgu.de](mailto:marc.ditzhaus@ovgu.de)

Markus Pauly, [markus.pauly@tu-dortmund.de](mailto:markus.pauly@tu-dortmund.de)


## License
Please look at [License](./LICENSE)

# This is the data analysis script
# @author Marléne Baumeister
# the function QMANOVA() can be used to run the test


library(car)
library(GGally)
library(stringr)
library(HSAUR)

library(expm)
library(mltools)
library(data.table)
library(cIRT)
library(MASS)
library(parallel)
library(rlecuyer) 
library(MNM)
library(MANOVA.RM)

data("skulls")
summary(skulls)

skulls <- skulls[1:90,]
levels(skulls$epoch)<- c("4000 BC", "3300 BC", "1850 BC", NA, NA)
  c(levels(skulls$epoch)[1:3], NA, NA)

ggpairs(data = skulls, 
        columns = 2:5, 
        title = "", 
        ggplot2::aes(colour = epoch, alpha = 0.5))

skulls.long <- reshape(skulls,
                       varying = names(skulls)[-1],
                       times = names(skulls)[-1],
                       v.names = "value",
                       timevar = "measure",
                       direction = "long")
row.names(skulls.long) <- 1:nrow(skulls.long)
names(skulls.long)[1] <- "Epoch" 


ggplot(skulls.long,
       aes(y = measure, x = value, fill = Epoch)) + 
  geom_boxplot() +
  theme_bw() + 
  scale_fill_grey(start = 0.35) + 
  scale_y_discrete(name = "Measures") + 
  scale_x_continuous(name = "Length in mm") + 
  theme(text = element_text(size = 16), )


ggplot(skulls.long,
       aes(x = value, fill = Epoch)) + 
  geom_density(alpha = 0.5) +
  theme_bw() + 
  scale_fill_grey() + 
  scale_x_continuous(name = "Measures in mm", limits = c(40, 160)) + 
  scale_y_continuous(name = "Density") + 
  facet_wrap(vars(measure)) + 
  theme(text = element_text(size = 16), legend.position = "bottom")


skulls_list <- list(skulls[which(skulls$epoch == "c4000BC"), -1],
                    skulls[which(skulls$epoch == "c3300BC"), -1],
                    skulls[which(skulls$epoch == "c1850BC"), -1])


source("teststatistic.R")

#' type of test statistic 
stat <- c("MATS", "ATS")

#'type of plug-in estimators
est <- c("int", "boot", "kernel")

statsettings <- expand.grid(stat, est)
names(statsettings) <- c("test_statistic", "estimator")

make_layout <- function(d, k) {
  #'calculates the layout matrix dependent from the dimension and the groups
  #'param:
  #'d: dimension
  #'k: groups
  #'
  layout <- kronecker(diag(k)-1/k*matrix(1, k, k), 
                      diag(d)) 
  return(layout)
}




QMANOVA <- function(data, data.wide, statsettings, nboot, alpha) {
  #' calculates the test decisions of a resampling test
  #' param:
  #' data: dataset, list of number of groups n*d matrices,
  #' data.wide: data in another format which is needes by the comparing model
  #' statsettings: configuration scenarios of 
  #' the test statistics the plug-in estimators
  #' layout: layout matrix which characterizes the null hypothesis
  #' nboot: number of bootstrap iterations
  #' alpha: significance level of the test
  #'
  d <- ncol(data[[1]])
  k <- length(data)
  nvector <- sapply(data, nrow)
  #' layout
  layout <- make_layout(d, k)
  #' calculate the test statistics, the median and the covariance
  input <- teststat(data, statsettings, d, k, nvector, layout)
  teststatistic <- input[[1]]
  median <- input[[2]]
  #' calculate the bootstrap quantiles
  teststats <- matrix(0, nrow = nboot, ncol = length(teststatistic))
  for (j in 1:nboot) {
    #' nonparametric bootstrap
    groupnon <- list()
    for (i in 1:k) {
      #' sample rows of the original data frame with replacement
      choice <- sample(nrow(data[[i]]), replace = TRUE)
      #' choose the sampled rows from the data
      groupnon[[i]] <- data[[i]][choice,]
    }
    teststats[j, ] <- teststat(groupnon,
                               statsettings,
                               d,
                               k,
                               nvector,
                               layout,
                               q = median)[[1]]
    
  } # for with j
  bootquant <- apply(teststats, 2, quantile, 1-alpha, na.rm = TRUE)
  ecdf_boot <- apply(teststats, 2, ecdf)
  
  #' calculate the test decisions
  param <- matrix(rep(17, nrow(statsettings)),
                  nrow = 3,
                  ncol = 8)
  param[1, 1:nrow(statsettings)] <- teststatistic
  param[2, 1:nrow(statsettings)] <- bootquant
  for (j in 1:nrow(statsettings)) {
    param[3, j] <- 1- ecdf_boot[[j]](teststatistic[j]) 
  }
  #' do the comparing models from MANOVA.RM
  #' modify data
  #data.wide <- data.frame(rbind(data[[1]], data[[2]], data[[3]]), 
                         # group = as.factor(c(rep(1,nvector[1]), 
                                             # rep(2, nvector[2]),
                                             # rep(3, nvector[3]))))
  #' p-values
  param[3, 7] <- MANOVA.wide(cbind(mb, bh, bl, nh) ~ epoch, 
                            data = data.wide, 
                            iter = nboot,
                            resampling = "paramBS",
                            alpha = alpha)$resampling[2]
  param[3, 8] <- MANOVA.wide(cbind(mb, bh, bl, nh) ~ epoch, 
                            data = data.wide, 
                            iter = nboot,
                            resampling = "WildBS",
                            alpha = alpha)$resampling[2]
  param <- data.frame(param)
  names(param) <- apply(statsettings, 1, paste, sep = "", collapse = '')
  return(param)
} # function



QMANOVA(skulls_list, skulls, statsettings, 2000, 0.05)

skulls1 <- skulls[1:60, ]
skulls1$epoch <- factor(skulls1$epoch)
QMANOVA(list(skulls_list[[1]], skulls_list[[2]]) , skulls1 , 
        statsettings, 2000, 0.05)

skulls2 <- skulls[31:90, ]
skulls2$epoch <- factor(skulls2$epoch)
QMANOVA(list(skulls_list[[2]], skulls_list[[3]]) , skulls2, 
        statsettings, 2000, 0.05)

skulls3 <- rbind(skulls[1:30, ], 
                 skulls[61:90, ])
skulls3$epoch <- factor(skulls3$epoch)
QMANOVA(list(skulls_list[[1]], skulls_list[[3]]) , skulls3, 
        statsettings, 2000, 0.05)

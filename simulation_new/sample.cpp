// [[Rcpp::depends(RcppArmadillo)]]
// written by Steffen Maletz maletz@statistik.tu-dortmund.de
#include <RcppArmadillo.h>
using namespace Rcpp;


// [[Rcpp::export]]
arma::vec sample_mPoisson(arma::vec expectation, Rcpp::S4 copula_object){
  Rcpp::Function copula_sample = Rcpp::Environment("package:copula")["rCopula"];
  arma::vec observations(expectation.n_elem, arma::fill::zeros);
  arma::uvec finished(expectation.n_elem, arma::fill::value(0));
  arma::vec copula_values(expectation.n_elem);
  arma::vec temp(expectation.n_elem, arma::fill::zeros);
  
  while (any(finished == 0))
  {
    copula_values = as<arma::vec>(copula_sample(1, copula_object));
    temp -= log(copula_values);
    finished = 1 * (temp > expectation);
    observations(arma::find(finished == 0)) += 1;
  }
  return observations;
}


